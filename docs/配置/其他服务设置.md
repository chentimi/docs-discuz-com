﻿# 其他服务设置
## 微信小商店配置
### 概述
 Discuz! Q 支持将您的微信小商店商品关联到 Discuz! Q 中并进行发帖售卖，实现电商盈利。本文将指导您如何配置  Discuz! Q 微信小商店并发布微信小商店商品帖。
 
 ### 前提条件
 
- 已登录 Discuz! Q 后台。
- 已[开通微信小商店](https://developers.weixin.qq.com/doc/ministore/minishopquickstart/introduction.html)。

### 操作步骤
#### 步骤1：获取配置信息
1. 登录[微信小商店控制台](https://shop.weixin.qq.com/)。
2. 依次单击侧边栏 **店铺管理**->**基础信息**。
3. 在基础信息页账号信息栏获取您的AppID(小程序ID)与AppSecret(小程序密钥)信息。如下图所示：

![](https://qcloudimg.tencent-cloud.cn/raw/3e63a5866fd0febdfaa9a831b8ae4ecd.png)

:::tip
微信小商店出于安全考虑，AppSecret(小程序密钥)不再被明文保存，忘记密钥需点击**重置**后才可获取新的 AppSecret 信息。
:::

#### 步骤2：配置微信小商店
1. 登录 Discuz! Q 后台并在**全局->其他服务设置**处单击**配置**。如下图所示：
![](https://qcloudimg.tencent-cloud.cn/raw/c8d92653171334e749dad4cd6ab895c9.png)

2. 在微信小商店配置页2，填写您**步骤1**中获取到的配置信息。如下图所示：
![](https://qcloudimg.tencent-cloud.cn/raw/6e32a451ecb67b685bb6ce70c2dd5245.png)

- appid：填写您或获取到的AppID(小程序ID)信息。
- secret key：填写您或获取到的AppSecret(小程序密钥)信息。
- 小店介绍：可自定义填写，用于介绍您的微信小商店。

3. 单击**提交**，即可完成配置。

#### 步骤3：发布微信小商店商品帖
1. 在 Discuz! Q 前台，单击**发布**。
2. 在文本编辑器中，单击购物车图标。如下图所示：

![](https://qcloudimg.tencent-cloud.cn/raw/59f4f73d791ccf433cf81a0a674d2006.png)

3. 在弹出的添加商品窗口中，勾选已在微信小商店发布的商品。如下图所示：
![](https://qcloudimg.tencent-cloud.cn/raw/e21b934bbdc7df9993f9f75611ec80ec.png)

4. 单击**确认**。
5. 完成编辑后，单击**发布**即可完成操作。

#### 效果展示

![](https://qcloudimg.tencent-cloud.cn/raw/c28df93ab75e51170a2ab51c7a79a096.png)





















 
 
 