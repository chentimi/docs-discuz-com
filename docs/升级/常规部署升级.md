# 常规部署方式升级
<h2> 概述</h2> 

本文档将指导您如何升级使用常规部署方式的 Discuz! Q 。

::: tip
常规部署方式升级，适用于：基于手动配置的环境、基于宝塔、基于镜像部署方式。
::: 

<h2>  前提条件</h2> 

已使用基于手动配置的环境、基于宝塔、基于镜像部署方式部署 Discuz! Q。

<h2> 操作指南</h2>

常规部署方式支持以下两种升级方式，您可以根据您的具体需求进行选择。

## 使用 dl.php 自动升级方式（推荐）


1. 使用浏览器访问 `http(s)://<已安装部署成功的网址>/dl.php` ,如官网 `https://discuz.chat` 则为 `https://discuz.chat/dl.php` 。如下图所示：
![](https://main.qcloudimg.com/raw/2d3ecf300f7807d3ae265c300f706293.png)

::: tip

- 如果左上角 Logo 处没有版本号信息，请下载并替换最新版本的 [安装工具](https://discuzq-docs-1258344699.cos.ap-guangzhou.myqcloud.com/dl.php)，将下载的 `dl.php` 文件放置于 `public` 目录下。

- 为了安全，`dl.php` 每次运行完成后，会在其所在目录下生成 `dl.php.lock` 锁文件，升级前请先删除此文件。`dl.php.lock`位于`public`目录下。

::: 

2. 单击【下一步】，Discuz! Q 将会对升级进行检查，检查成功后，在计划安装版本页面，选择需要升级的版本。如下图所示：

![](https://main.qcloudimg.com/raw/f1936d438b708483659ab95caa1744a4.png)

::: tip
- 选择下载的版本号必须大于当前安装版本号 ，否则无法继续。
- 默认勾选【升级前删除原来的静态文件目录】，可根据实际需求进行选择。
::: 

3. 单击【下一步】, Discuz! Q 将会自动执行升级。升级成功将会提示以下信息。如下图所示：

![](https://main.qcloudimg.com/raw/d01c06ff8ed24625cd882866c7da5295.png)

4. 单击下一步完成升级。

## 手动升级方式
手动升级版本方式请根据您的现有版本与所需升级到的版本进行选择：

::: tip
- 以下操作以使用 Nginx 服务为例。如您使用其他 Web 服务，建议您使用自动升级方式，或参考以下操作进行升级。
- 以下升级命令需按从上至下依次执行，否则将可能导致升级失败。
::: 

#### 升级到v3.0.210926

1. 在终端中，进入您的 `discuz` 的主目录并运行以下命令。

```
php disco migrate --force
php disco init:activity
```

2. 重启 Nignx 服务即可完成升级。

#### 升级到v3.0.210917

1. 在终端中，进入您的 `discuz` 的主目录并运行以下命令。

```
php disco migrate --force
php disco upgrade:issue_at
```

2. 重启 Nignx 服务即可完成升级。

#### 升级到v3.0.210902

1. 在终端中，进入您的 `discuz` 的主目录并运行以下命令。

```
php disco migrate --force
php disco upgrade:removeNicknameBlank
```

2. 重启 Nignx 服务即可完成升级。

#### 升级到v3.0.210819

1. 在终端中，进入您的 `discuz` 的主目录并运行以下命令。

```
php disco migrate --force
php disco upgrade:addVotePermission
```

2. 重启 Nignx 服务即可完成升级。

#### 升级到v3.0.210729

1. 在终端中，进入您的 `discuz` 的主目录并运行以下命令。

```
php disco migrate --force
php disco upgrade:nicknameDuplicateRemoval
```

2. 重启 Nignx 服务即可完成升级。

#### 升级到v3.0.210715

1. 在终端中，进入您的 `discuz` 的主目录并运行以下命令。

```
php disco migrate --force
php disco notifications:migration
```

2. 重启 Nignx 服务即可完成升级。

#### 升级到v3.0.210713

1. 在终端中，进入您的 `discuz` 的主目录并运行以下命令。

```
php disco migrate --force
php disco upgrade:stopWordsNicknameSet
```

2. 重启 Nignx 服务即可完成升级。

#### 升级到v3.0.210706

1. 在终端中，进入您的 `discuz` 的主目录并运行以下命令。

```
php disco migrate --force
php disco postForeignKey:update
```

2. 重启 Nignx 服务即可完成升级。

#### 升级到v3.0.210628

1. 在终端中，进入您的 `discuz` 的主目录并运行以下命令。

```
php disco migrate --force
php disco upgrade:addUnpaidPermission
```

2. 重启 Nignx 服务即可完成升级。

#### 升级到v3.0.210609

1. 在终端中，进入您的 discuz 的主目录并运行以下命令:

```
php disco migrate --force
php disco upgrade:upgradeOldPermissionData
php disco upgrade:nicknameSet
php disco upgrade:bindType
php disco upgrade:questionMigration
php disco upgrade:noticeAdd
php disco thread:migration
```

2. 修改您的网站主目录配置文件 `config.php`, 查找字段 `'disks' => `的配置项添加如下内容：

```
 'background' =>[
                    'driver'    =>  'local',
                    'root'    =>  storage_path('app/public/background'),
                    'url'    =>  'background',
                    'visibility'    =>  'public',
                ],
'background_cos' => [
                    'driver' => 'cos',
                    'root' => storage_path('app/public/background'),
                    'url' => 'background',
                    'visibility' => 'public',
                ]
```

3. 重启 Nignx 服务即可完成升级。

#### 升级到v2.3.210528

1. 在终端中，进入您的 `discuz` 的主目录并运行以下命令。

```
php disco migrate --force
php disco upgrade:noticeAdd
```

2. 重启 Nignx 服务即可完成升级。

#### 升级到v2.3.210412
1. 在终端中，进入您的 `discuz` 的主目录并运行以下命令。

```
php disco migrate --force
php disco upgrade:noticeAdd
php disco upgrade:noticeIteration
```

2. 重启 Nignx 服务即可完成升级。

#### 升级到v2.3.210207
1. 在终端中，进入您的 `discuz` 的主目录并运行以下命令即可完成升级。

```
php disco migrate --force
php disco upgrade:notice
php disco upgrade:notice-iteration
php disco upgrade:groupPermissionAdd
php disco upgrade:settingAdd
```

:::tip
升级成功前如您已设置为红色主题，升级成功后将默认回退为蓝色主题，您需在 Discuz! Q 管理后台重新设置为红色主题。
:::

#### 升级到v2.3.210202
1. 在终端中，进入您的 `discuz` 的主目录并运行以下命令。

```
php disco migrate --force
php disco upgrade:notice
php disco upgrade:notice-iteration
php disco upgrade:groupPermissionAdd
php disco upgrade:settingAdd
```

2. 重启 Nignx 服务即可完成升级。

#### 升级到v2.1.201113

1. 在终端中，进入您的 `discuz` 的主目录并运行以下命令。

```
php disco migrate --force
php disco upgrade:split-permissions
php disco site:switch
```

2. 修改您的 Nginx 配置文件 `nginx.conf` 的 `index` 配置项，修改为以下参数。

```
index index.php index.html;
```
3. 重启 Nignx 服务即可完成升级。


#### 升级到v2.1.201029

1. 在终端中，进入您的 discuz 的主目录并运行以下命令:

```
php disco migrate --force
php disco upgrade:notice
```

2. 修改网站主目录下的 `config.php` 配置文件, 查找字段 `'providers' => `的配置修改为以下内容：

```
 'providers' => [
        App\Formatter\FormatterServiceProvider::class,
        App\Passport\Oauth2ServiceProvider::class,
        App\Providers\AppServiceProvider::class,
        App\Providers\AttachmentServiceProvider::class,
        App\Providers\CategoryServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\OrderServiceProvider::class,
        App\Providers\PostServiceProvider::class,
        App\Providers\SettingsServiceProvider::class,
        App\Providers\ThreadServiceProvider::class,
        App\Providers\UserServiceProvider::class,
        App\Providers\DialogMessageServiceProvider::class,
        App\Providers\QuestionServiceProvider::class,
    ],
```

3. 重启 Nignx 服务即可完成升级。

#### 升级到v2.0.201001、v1.0.200911、v1.0.200821、v1.0.200806、v1.0.200723、v1.0.200715、v1.0.200707、v1.0.200705、v0.5.200522、v0.5.200424
1. 在终端中，进入您的 discuz 的主目录并运行以下命令:
```
php disco migrate --force
```
2. 重启 Nignx 服务即可完成升级。

#### 升级到v1.0.200710
1.需配置PHP，启用 `exif` 扩展。

2. 在终端中，进入您的 `discuz` 的主目录并运行以下命令:
```
php disco migrate --force
```
3. 重启 Nignx 服务即可完成升级。

#### 升级到v1.0.200703

1. 修改 Web 服务配置，修改内容请查看：[Ngnix服务](https://discuz.com/docs/Linux%20%E4%B8%BB%E6%9C%BA.html#nginx)。
2. 在终端中，进入您的 `discuz` 的主目录并运行以下命令:
```
php disco migrate --force
php disco upgrade:category-permission 
php disco upgrade:videoSize
php disco upgrade:notice
```
3. 修改网站主目录下的 `config.php` 配置文件，将字段 `'providers' =>` 的配置项修改为以下内容：
```
'providers' => [
    App\Formatter\FormatterServiceProvider::class,
    App\Passport\Oauth2ServiceProvider::class,
    App\Providers\AppServiceProvider::class,
    App\Providers\AttachmentServiceProvider::class,
    App\Providers\CategoryServiceProvider::class,
    App\Providers\EventServiceProvider::class,
    App\Providers\OrderServiceProvider::class,
    App\Providers\PostServiceProvider::class,
    App\Providers\SettingsServiceProvider::class,
    App\Providers\ThreadServiceProvider::class,
    App\Providers\UserServiceProvider::class,
    App\Providers\DialogMessageServiceProvider::class,
],
```
4. 重启 Nignx 服务即可完成升级。

#### 升级到v0.5.200508
1. 在终端中，进入您的 `discuz` 的主目录并运行以下命令:
```
php disco migrate --force
```
2. 重启 Nignx 服务即可完成升级。

::: tip
如果运行以上脚本时出现错误，请下载[此文件](https://discuzq-docs-1258344699.cos.ap-guangzhou.myqcloud.com/2020_04_14_104730_update_users_table.php)，替换 `discuz` 主目录下的 `database/migrations` 目录下的同名文件，然后再次执行 `php disco migrate --force`.
::: 

#### 升级到v0.5.200410

1. 附件图片统一接入对象存储服务后，需要重新配置”后台-全局-腾讯云设置-对象存储“中的“访问域名”。访问域名获取方式为：“腾讯云-对象存储-存储桶列表-配置管理-基本配置”中的“访问域名”（详见[使用手册](https://dl.discuz.chat/dzq_userguide_latest.pdf)“2.2.7.6对象存储”部分说明）。

2. 修改网站主目录下的 `config.php` 配置文件，将字段 `attachment` 修改为以下配置：
```
'attachment' => [
```
修改为：
```
'attachment_cos' => [
```

并在此配置之前加入以下配置：

```
'attachment' => [
    'driver' => 'local',
    'root'   => storage_path('app'),
    'url'    => 'attachment'
],
```

最终配置如下所示：
```
'attachment' => [
    'driver' => 'local',
    'root'   => storage_path('app'),
    'url'    => 'attachment'
],
'attachment_cos' => [
    'driver' => 'cos',
    'root'   => storage_path('app/public/attachment'),
    'url'    => 'attachment'
],
```

3. 重启 Nignx 服务即可完成升级。

#### 升级到v0.4.200331
按 v0.4.200331 升级的要求完成数据库升级和配置文件修改即可完成升级。


#### 从任意版本升级到 v0.4.200331
1. 在终端中，进入您的 `discuz` 的主目录并运行以下命令:
```
php disco migrate --force
```
2. 修改网站主目录下的 `config.php` 配置文件，在`attachment`字段之前，加入以下配置：

```
'avatar_cos' => [
  'driver' => 'cos',
  'root' => storage_path('app/public/avatars'),
  'url' => 'avatar',
  'visibility' => 'public',
],

```

#### 从任意版本升级到 v0.4.200327
1. 需要在完成以上步骤后，在终端中，进入您的 `discuz` 的主目录并运行以下命令:、

```
php disco migrate --force
```

2. 重启 Nignx 服务即可完成升级。
