# 关于 Discuz! Q
Discuz! Q 拥有完全开源、提供丰富接口、前后端分离、轻量化、数据独立可控、敏捷上云、快速变现七大能力。通过这些能力，能够帮助创业者更高效的上线内容产品，让信息能高效准确的分享与传播，流量变现能更加简单快捷。在某种意义上，我们希望能够重塑移动社区的用户体验，提供一种更可靠的信息连接与知识传播的方式。

##  Discuz! Q 官网

**[Discuz! Q 官方站](https://discuz.com/)**

**[Discuz! Q 官方演示站](https://discuz.chat/)**

## 感谢

Discuz! Q 项目是一个从 0 到 1 的全新项目，如果每一个功能都从零开始编写代码，将是极为庞大的工程。想想 Discuz! X ，代码量依赖 10 多年的时间的积累，才完善出各种工具类、自己的框架及插件机制等。

在此背景下，我们借助了开源的力量，才得以快速构建出 Discuz! Q。以下是整个 Discuz! Q 中所用到的技术栈，在此特别感谢他们：


| <p style="width: 300px;">名称</p> | <p style="width: 570px;">官网</p> |
|---------|---------|
| 康创联盛 | [![](https://main.qcloudimg.com/raw/3cbe594672cdd905cbb1cf6501b41fea.png)](https://www.comsenz-service.com/portal.php) |
| Taro | [![](https://main.qcloudimg.com/raw/452a6ac73d856d9971647d6e2f86d0ff.png)](https://taro.zone/) |
| Next.js | [![](https://main.qcloudimg.com/raw/56fb4cd4a0c1a24f95c6df66f4cf12c8.png)](https://www.nextjs.cn/) |
| Symfony | [![](https://main.qcloudimg.com/raw/ad786873f80fdab4f7db0ba08f54c023.png)](https://symfony.com/) |
| Laminas | [![](https://main.qcloudimg.com/raw/ab5835c1424e1707addb3d9eaf1ce50b.png)](https://getlaminas.org/) |
| FastRoute |[ https://github.com/nikic/FastRoute](https://github.com/nikic/FastRoute) |
| Guzzle |[ http://guzzlephp.org/](http://guzzlephp.org/) |
| thephpleague |[ https://thephpleague.com/](https://thephpleague.com/) |
| s9etextformatter |[ https://s9etextformatter.readthedocs.io/](https://s9etextformatter.readthedocs.io/) |
| overtrue |[ https://overtrue.me/](https://overtrue.me/) |
| monolog |[ https://github.com/Seldaek/monolog](https://github.com/Seldaek/monolog) |
| intervention |[ http://image.intervention.io/](http://image.intervention.io/) |
| whoops |[ https://github.com/filp/whoops](https://github.com/filp/whoops) |
| vue |[ https://youzan.github.io/vant/#/zh-CN/](https://youzan.github.io/vant/#/zh-CN/) |
| Vant |[ http://image.intervention.io/](http://image.intervention.io/) |
| element-ui |[ https://element.eleme.cn/#/zh-CN](https://element.eleme.cn/#/zh-CN) |

## 鸣谢

Discuz! Q 的成长离不开社区中 Q 友的陪伴与支持，在 Discuz! Q 2.x 迭代至 Discuz! Q 3.0 过程中，我们 26 位体验官深度地参与到 Discuz! Q 3.0 开发版的体验，为我们的抓虫提报 BUG，为我们提功能优化建议。尽管 BUG 永无止境，但体验官的真诚与热情，让我们有了更大的勇气直面 BUG。是协作共建，让我们的 Discuz! Q 更加健壮，让更多人受益。

在当前 Discuz! Q 项目已经开源的情况下，也感谢参与开源社区，持续推动产品和项目建设的各位参与者，一同将项目建设的更加完善！