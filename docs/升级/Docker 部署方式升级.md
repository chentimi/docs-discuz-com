# Docker容器部署方式升级
<h2>概述</h2> 

本文档将指导您如何升级使用 Docker 部署方式的 Discuz! Q 。

::: tip
执行此操作前，请先将数据保存到 Docker 容器外部，再进行升级。在升级时原 Docker 容器需先停止并删除，未保存数据到外部将导致数据丢失，请确保已将数据保存到外部。
::: 

<h2>前提条件</h2> 

- 已使用 Docker 部署方式成功部署 Discuz! Q。
- 已将数据保存到了 Docker 容器外部。

<h2>操作指南</h2> 

1.在升级前，需要将原容器先停止并删除，请在终端中输入以下命令：

```
docker stop <容器 ID>
docker rm <容器 ID>
```
::: tip
其中的<容器 ID>，可以通过在终端中输入`docker ps` 命令查询。
::: 

2. 使用以下命令下载最新版本镜像文件:

```
docker pull ccr.ccs.tencentyun.com/discuzq/dzq:latest
```

3. 重新启动 Docker 容器即可。


如果需要执行升级文档中要求的其它升级命令，请先使用以下命令登录容器：

```
docker exec -it <容器 ID> /bin/bash
```

即可执行升级文档中要求的相关的命令，例如：

```
 cd /var/www/discuz
 php disco migrate --force
```

::: tip
如果您想对容器进行其他配置，详情请查看：[Docker 容器配置说明](https://discuz.com/docs/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98.html#%E5%AE%B9%E5%99%A8%E7%9A%84%E6%9B%B4%E5%A4%9A%E9%85%8D%E7%BD%AE%E8%AF%B4%E6%98%8E)。
::: 