# 开源应用中心安装（推荐）

开源应用中心（OAC）是聚集开发者以及中小企业使用的开源应用的平台。平台内的应用已经与腾讯云完成深度的集成整合，您只需通过单击鼠标即可体验在云上快速的部署，并且使用应用的最新特性。本文将指导您如何使用腾讯云开源应用中心能力快速开通安装 Discuz! Q。

### 步骤1：登录注册
[注册腾讯云账号](https://cloud.tencent.com/register?s_url=https%3A%2F%2Fcloud.tencent.com%2F)，并进行实名认证。
- 若您已在腾讯云注册，可忽略此步骤。
- 若您未在腾讯云注册，请参考相关文档 [注册腾讯云](https://cloud.tencent.com/document/product/378/17985)。

### 步骤2：正式开通 Discuz! Q 
1. 登录腾讯云 [开源应用中心](https://app.cloud.tencent.com/)，在搜索窗口中查询 Discuz! Q 。如下图所示：

![](https://main.qcloudimg.com/raw/5746549fc5631c4f97557259fde673f3.png)

2. 在弹出的 “应用详情” 窗口中，单击【正式开通】。如下图所示：
![](https://main.qcloudimg.com/raw/5be8ab22c3e09a4dc65ecfea1ffc58c0.png)

### 步骤3：一键部署应用
1. 在弹出的 “一键部署应用” 窗口中，进入 “开通授权” 配置步骤，已默认选择相应开通权限，并单击【下一步】。如下图所示：
![](https://main.qcloudimg.com/raw/f8eb52f069761c2dea15a19ad34553be.png)
2. 进入 “环境信息” 配置步骤，请根据页面提示配置环境信息，并单击【下一步】。如下图所示：

![](https://main.qcloudimg.com/raw/baaafcbd202b52a6e0c5f8372189cad6.png)
 - **环境类型**：默认为 “新建环境”。
 - **地域**：目前可选择上海或广州，请根据您的实际情况选择。
 - **计费方式**：默认为 “按量计费”。
 - **免费资源**：默认勾选 “开启免费资源”。
 - 关**联云上资源**：如您已有实例，可根据您实际情况选择实例，如没有实例，则默认创建新的实例，同时需要勾选 “我已知悉，新建实例将产生费用”。
3. 进入“应用配置” 步骤，确认您的登录信息，并单击【立即开通】。如下图所示：
![](https://main.qcloudimg.com/raw/8d79ace32acc96b3501010ae3ae880a2.png)

:::tip
开源应用开通 Discuz! Q 预计需要5 - 10分钟，请耐心等待。
:::

### 步骤4：访问 Discuz! Q 
1. 部署安装 Discuz! Q 应用实例成功后，登录 [开源应用中心管理控制台](https://console.cloud.tencent.com/oac/list)，进入 “应用列表” 管理页面，即可查看已购买的应用实例。
2. 选择您需要进行管理的正式商品应用实例并单击【管理】。如下图所示：
![](https://main.qcloudimg.com/raw/439fa5d0a574c1acf1685dd99e9c8988.png)
3. 进入正式商品应用实例详情页，即可查看应用实例的访问信息。

:::tip
开源应用中心详情操作指南和定价说明请参见：[开源应用中心产品文档](https://cloud.tencent.com/document/product/1298) 。
:::