---
sidebarDepth: 2
---
# Linux 主机安装

## 基于手动配置的环境

###  Apache 

####  操作场景

本文档将指导您如何在已安装 Apache 服务的 Linux 服务器上安装部署 Discuz! Q。 

::: tip
- 若您已知晓如何安装部署 Discuz! Q，您可以直接[单击此处](https://dl.discuz.chat/dzq_latest_install.zip)下载 Discuz! Q 进行安装部署。
- 本文档以 Apache 2.4.46 版本、PHP 7.3.23、 CentOS 7.6 为例。 
:::

####  前提条件

- 服务器已安装的 PHP 版本为 7.2.5 及以上(暂不推荐使用7.4)，数据库使用 MySQL 5.7.9 版本以上或 MariaDB 10.2 以上。
- 已成功登录 Linux 服务器。    

#### 操作步骤

#### 步骤一：配置PHP

#### 安装扩展

::: tip

- Discuz! Q 依赖于 `BCMath` `Ctype` `Curl` `Dom` `Fileinfo` `GD` `JSON` `Mbstring` `Exif` `OpenSSL` `PDO` `PDO_mysql` `Tokenizer` `XML` `Zip`扩展插件，在 PHP 中需开启以上扩展。
- 以下操作因为系统版本，软件版本的不同，操作上会有所差异，请根据您的具体情况进行开启对应函数，以下操作仅提供示例。
  ::: 

1. 查看 PHP 已安装扩展。在终端中，可以输入 `php -m` 查看已安装扩展。

2. 安装缺失扩展。在终端中，您可以根据查询结果安装对应缺失的扩展。

   - 如缺失 `Exif` 扩展，可以使用以下命令进行编译安装扩展 `Exif `扩展。

::: tip
以下路径仅提供示例。请根据实际情况进行输入。
::: 

```
# 使用 cd 命令进入 php 的源码的 exif 模块目录。
cd /root/lamp1.7/src/php-7.3.23/ext/exif/

# 执行 phpize 脚本。
/usr/local/php/bin/phpize

# 编译配置。
./configure --with-php-config=/usr/local/php/bin/php-config

# 编译安装 exif 模块。
make && make install
```

1. 配置`php.ini`文件。可在终端中输入`php --ini`查看`php.ini`配置文件路径。如下图所示：

![](https://main.qcloudimg.com/raw/149f2f45d6c305fa8710fe265c6be57d.png)

2. 您可通过`vim `命令，或使用 WinSCP 工具，根据查询到的文件路径，打开`php.ini`文件。
3. 编辑`php.ini` 文件，去掉以下字段前面的`;`分号，并保存`php.ini` 文件。如下所示：

```
extension=exif    
exif.encode_unicode = ISO-8859-15
exif.decode_unicode_motorola = UCS-2BE
exif.decode_unicode_intel    = UCS-2LE
exif.encode_jis =
exif.decode_jis_motorola = JIS
exif.decode_jis_intel    = JIS=
```

4. 重启 PHP 与 Apache 服务。
::: tip
您可参考命令`service php-fpm restart`与 `service httpd restart`重启 PHP 与 Apache 服务。
::: 

#### 启用 PHP 函数

::: tip

-  Discuz！Q 依赖于 `symlink`、`readlink`、`putenv`、`realpath`、`shell_exec` 函数，在PHP中需开启以上函数。
-  以下操作因为系统版本，软件版本的不同，操作上和命令上会有所差异，请根据您的具体情况开启 `mod_rewrite` ，以下操作仅提供示例。
   ::: 

1. 打开并编辑 `php.ini`文件。查找 `disable_functions`字段，并删除禁用掉的`symlink`、`readlink`、 `putenv`、 `realpath`、`shell_exec` 函数。如下所示：

```
disable_functions = passthru,exec,system,chroot,chgrp,chown,shell_exec,proc_open,proc_get_status,popen,ini_alter,ini_restore,dl,openlog,syslog,popepassthru,stream_socket_server
```

2. 保存 `php.ini`文件。
3. 重启 PHP 与 Apache 服务。

#### 步骤二：配置 Apache 

::: tip

- Discuz！Q 依赖于 `mod_rewrite` 模块。请确保已经启用 `mod_rewrite`，以下操作将指导您启动 `mod_rewrite`。
- 以下操作因为系统版本，软件版本的不同，操作上和命令上会有所差异，请根据您的具体情况开启 `mod_rewrite` ，以下操作仅提供示例。
  ::: 

#### 启用 mod_rewrite

1. 检查 `mod_rewrite` 是否启用。您可以在终端中输入 `httpd -M` 查看已安装模块。
2. 如果查询结果中无 `rewrite_module` 字段，您可以在 `conf` 目录的 `httpd.conf` 文件中，查找以下字段，并去掉字段前`#` 符号。如下所示：

```
LoadModule rewrite_module modules/mod_rewrite.so
```

3. 定位到 `<directory />` 字段，并修改内容为如下内容：

```
<directory />
        Options All
        AllowOverride All
        DirectoryIndex index.php index.html
</directory>
```

4. 重启 Apache 服务。

#### 步骤三：下载并解压 Discuz！Q 安装包

1. 您可以通过以下命令在网站主目录下创建 Discuz！Q 网站目录，并下载 Discuz！Q 安装包与解压。

```
# 创建Discuz！Q 网站目录，目录名可自定义，此处以 discuz 为例。
mkdir discuz

# 进入 discuz 目录
cd discuz

# 下载 Discuz！Q 安装包
wget -c https://dl.discuz.chat/dzq_latest_install.zip

# 解压 Discuz！Q 安装包
unzip dzq_latest_install.zip
```

#### 步骤四：初始化安装 Discuz! Q

1. 打开本地浏览器，访问 `http://<绑定网站的域名名称>/dl.php` 。如下图所示：

![](https://main.qcloudimg.com/raw/d571ed3234565af42c508666b827ca37.png)


::: tip

- 站点如需部署 SSL 证书，使用 HTTPS 协议进行站点访问。请在以下操作之前部署安装 SSL 证书，并使用`https://<绑定网站的域名名称>/dl.php`  进行访问。部署安装 SSL 证书详情操作请参考： [Nginx 服务器证书安装](https://cloud.tencent.com/document/product/400/35244)。
- 如需购买 SSL 证书，可参考 [SSL 证书购买流程](https://cloud.tencent.com/document/product/400/7995)，请根据您的实际情况进行购买相关证书。腾讯云为 Discuz! Q 站点提供免费 SSL 证书申请。详情请查看：[域名型（DV）免费证书申请流程](https://cloud.tencent.com/document/product/400/6814)
  ::: 

2. 单击【下一步】，Discuz! Q 将自行进行站点检查。如有报错等问题，可参考 [常见问题](https://discuz.com/docs/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98.html) 进行排查处理。

3. Discuz! Q 检查完成后，需配置网站相关信息。如下图所示：

![](https://main.qcloudimg.com/raw/8e3ac52669180620b946750787b50c95.png)

- **站点名称**：请输入您的站点名称信息，可自定义。
- **MySQL 服务器地址**：请输入您的 MySQL 服务器地址。
- **数据库名称**：请输入您的数据库名称。
- **MySQL 用户名**：请输入您的数据库用户名。
- **MySQL 密码**：请输入您的数据库密码。
- **表前缀**：可选，可自定义数据库表前缀名称。默认不填。
- **设置管理员用户名**：请输入您 Discuz! Q 站点的管理员用户名。
- **设置管理员密码**：请输入您 Discuz! Q 站点的管理员密码。
- **管理员密码确认**：请再次输入您 Discuz! Q 站点的管理员密码。

4. 单击【下一步】。即可完成 DIscuz！Q 的安装部署。

#### 步骤五：添加计划任务

为保证 DIscuz！Q 站点功能的正常使用。您还需要在服务器添加计划任务。在终端中输入以下命令并执行。

```
php <网站主目录>/disco schedule:run >> /dev/null 2>&1
```

###  Nginx 

#### 操作场景

本文档将指导您如何在已安装 Nginx 服务的 Linux 服务器上安装部署 Discuz! Q。 

::: tip
- 若您已知晓如何安装部署 Discuz! Q，您可以直接[单击此处](https://dl.discuz.chat/dzq_latest_install.zip)下载 Discuz! Q 进行安装部署。
- 本文档以 Nginx 1.18.0 版本、PHP 7.3.23  、CentOS 7.6 为例。 
:::

#### 前提条件

- 服务器已安装的 PHP 版本为 7.2.5 及以上(暂不推荐使用7.4)，数据库使用 MySQL 5.7.9 版本以上或MariaDB 10.2以上。
- 已成功登录 Linux 服务器。    

#### 操作步骤

#### 步骤一：配置PHP

#### 安装扩展

::: tip

- Discuz! Q 依赖于 `BCMath` `Ctype` `Curl` `Dom` `Fileinfo` `GD` `JSON` `Mbstring` `Exif` `OpenSSL` `PDO` `PDO_mysql` `Tokenizer` `XML` `Zip`扩展插件，在 PHP 中需开启以上扩展。
- 以下操作因为系统版本，软件版本的不同，操作上会有所差异，请根据您的具体情况进行开启对应扩展，以下操作仅提供示例。
  :::

1. 查看 php 已安装扩展。在终端中，可以输入`php -m`查看已安装扩展。
2. 安装缺失扩展。在终端中，您可以根据查询结果安装对应缺失的扩展。
   - 如缺失`Exif`扩展。可以使用以下命令进行编译安装扩展 `Exif`扩展。

::: tip
以下路径仅提供示例。请根据实际情况进行输入。
:::

```
# 使用 cd 命令进入 php 的源码的exif模块目录。
cd /root/lnmp1.7/src/php-7.3.23/ext/exif/

# 执行 phpize 脚本。
/usr/local/php/bin/phpize

# 编译配置。
./configure --with-php-config=/usr/local/php/bin/php-config

# 编译安装 exif 模块。
make && make install
```

1. 配置`php.ini`文件。可在终端中输入`php --ini`查看`php.ini`配置文件路径。如下图所示：

![](https://main.qcloudimg.com/raw/149f2f45d6c305fa8710fe265c6be57d.png)

2. 您可通过`vim`命令，或使用 WinSCP 工具，根据查询到的文件路径，打开`php.ini`文件。
3. 编辑`php.ini` 文件，去掉以下字段前面的`;`分号，并保存`php.ini` 文件。如下所示

```
extension=exif    
exif.encode_unicode = ISO-8859-15
exif.decode_unicode_motorola = UCS-2BE
exif.decode_unicode_intel    = UCS-2LE
exif.encode_jis =
exif.decode_jis_motorola = JIS
exif.decode_jis_intel    = JIS=
```

3. 重启 PHP 与 Nginx 服务。
   ::: tip
   您可以参考`service php-fpm restart`与 `nginx -s reload`命令重启 PHP 与 Nginx 服务。
   ::: 

#### 步骤二:安装 PHP 函数

::: tip

- Discuz! Q 依赖于`symlink`、`readlink`、 `putenv`、 `realpath`、`shell_exec`函数，在PHP中需开启以上函数。
- 以下操作因为系统版本，软件版本的不同，操作上会有所差异，请根据您的具体情况进行开启对应函数，以下操作仅提供示例。
  ::: 

1. 打开并编辑 `php.ini`文件。查找 `disable_functions`字段，并删除禁用掉的`symlink`、`readlink`、 `putenv`、 `realpath`、`shell_exec` 函数。如下所示：

```
disable_functions = passthru,exec,system,chroot,chgrp,chown,shell_exec,proc_open,proc_get_status,popen,ini_alter,ini_restore,dl,openlog,syslog,popepassthru,stream_socket_server
```

2. 保存 `php.ini`文件。
3. 重启 PHP 与 Nginx 服务。



#### 步骤三：下载并解压 Discuz！Q 安装包

1. 您可以通过以下命令在网站主目录下创建 Discuz！Q 网站目录，下载 Discuz！Q 安装包与解压。

```
# 创建Discuz！Q 网站目录，目录名可自定义，此处以discuz为例。
mkdir discuz

#进入 discuz目录
cd discuz

#下载Discuz！Q 安装包
wget -c https://dl.discuz.chat/dzq_latest_install.zip

#解压 Discuz！Q 安装包
unzip dzq_latest_install.zip
```

#### 步骤四：配置 Nginx 

1. 查看 `nginx` 配置文件。您可以使用 `nginx -t` 命令查看 `nginx.conf` 配置文件路径。如下所示：

![](https://main.qcloudimg.com/raw/b4cfcbe58fd61b5d79e2e0af64cb5a35.png)


2. 您可通过 `vim` 命令，或使用 WinSCP 工具，打开 `nginx.conf` 配置文件进行以下配置。

::: warning
Nginx 必须包含以下配置。
::: 

请将`root`目录指向 `<站点主目录>/public`目录，同时一定要配置` index `和 `location /`，将所有的请求将引导到 `index.php` 。具体配置请参考如下：

#### root 配置

请确认 `root` 指向了安装好的 `public`目录，以下是示例，请按自己的实际配置设置。

```
root /home/www/discuz/public;
```

#### index 配置

请确认 index 的第一项是 index.php ，以下为示例。

```
index index.php index.html;
```

#### location 配置

请确认 location / 按如下配置，如果之前有相关配置，请替换：

```
    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }
```

#### Nginx 建议配置

建议添加以下配置，以启用 gzip 压缩,减少服务器资源损耗。

```
  gzip on;
  gzip_min_length 1024;
  gzip_types application/json text/html text/css application/x-javascript application/javascript application/vnd.api+json;
  gzip_disable "MSIE [1-6]\.";
  gzip_comp_level 2;
```

#### 步骤五：初始化安装 Discuz! Q

1. 打开本地浏览器，访问 `http://<绑定网站的域名名称>/dl.php` 。如下图所示：

![](https://main.qcloudimg.com/raw/d571ed3234565af42c508666b827ca37.png)


::: tip

- 站点如需部署 SSL 证书，使用 HTTPS 协议进行站点访问。请在以下操作之前部署安装 SSL 证书，并使用`https://<绑定网站的域名名称>/dl.php`  进行访问。部署安装 SSL 证书详情操作请参考： [Nginx 服务器证书安装](https://cloud.tencent.com/document/product/400/35244)。
- 如需购买 SSL 证书，可参考 [SSL 证书购买流程](https://cloud.tencent.com/document/product/400/7995)，请根据您的实际情况进行购买相关证书。腾讯云为 Discuz! Q 站点提供免费 SSL 证书申请。详情请查看：[域名型（DV）免费证书申请流程](https://cloud.tencent.com/document/product/400/6814)
  ::: 

2. 单击【下一步】，Discuz! Q 将自行进行站点检查。如有报错等问题，可参考 [常见问题](https://discuz.com/docs/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98.html) 进行排查处理。

3. Discuz! Q 检查完成后，需配置网站相关信息。如下图所示：

![](https://main.qcloudimg.com/raw/8e3ac52669180620b946750787b50c95.png)

- **站点名称**：请输入您的站点名称信息，可自定义。
- **MySQL 服务器地址**：请输入您的 MySQL 服务器地址。
- **数据库名称**：请输入您的数据库名称。
- **MySQL 用户名**：请输入您的数据库用户名。
- **MySQL 密码**：请输入您的数据库密码。
- **表前缀**：可选，可自定义数据库表前缀名称。默认不填。
- **设置管理员用户名**：请输入您 Discuz! Q 站点的管理员用户名。
- **设置管理员密码**：请输入您 Discuz! Q 站点的管理员密码。
- **管理员密码确认**：请再次输入您 Discuz! Q 站点的管理员密码。

4. 单击【下一步】。即可完成 DIscuz！Q 的安装部署。

#### 步骤六：添加计划任务

为保证 DIscuz！Q 站点功能的正常使用。您还需要在服务器添加计划任务。在终端中输入以下命令并执行。

```
php <网站主目录>/disco schedule:run >> /dev/null 2>&1
```


###  Caddy 

#### 操作场景

本文档将指导您如何在已安装 Caddy 的 Linux 服务器上安装部署 Discuz! Q。 

::: tip
- 若您已知晓如何安装部署 Discuz! Q，您可以直接[单击此处](https://dl.discuz.chat/dzq_latest_install.zip)下载 Discuz! Q 进行安装部署。
- 本文档以 Caddy 2.2.1 、PHP 7.3.25 、 CentOS 7.6 版本为例。
:::

#### 前提条件

- 服务器已安装的 PHP 版本为 7.2.5 及以上(暂不推荐使用7.4)，数据库使用 MySQL 5.7.9 版本以上或 MariaDB 10.2 以上。
- 已成功登录 Linux 服务器。

#### 操作步骤

#### 步骤一：配置 PHP

#### 安装扩展

::: tip

- Discuz! Q 依赖于 `BCMath` `Ctype` `Curl` `Dom` `Fileinfo` `GD` `JSON` `Mbstring` `Exif` `OpenSSL` `PDO` `PDO_mysql` `Tokenizer` `XML` `Zip` 扩展插件，在 PHP 中需开启以上扩展。
- 以下操作因为系统版本，软件版本的不同，操作上会有所差异，请根据您的具体情况进行开启对应扩展，以下操作仅提供示例。
  :::

1. 查看 `php` 已安装扩展。在终端中，可以输入 `php -m` 查看已安装扩展。
2. 在终端中，您可以根据查询结果安装对应缺失的扩展。
   - 如缺失`Exif`扩展。可以使用以下命令进行编译安装扩展 `Exif` 扩展。

::: tip
以下路径仅提供示例。请根据实际情况进行输入。
:::

```
# 使用 cd 命令进入 php 的源码的 exif 模块目录。
cd /root/lnmp1.7/src/php-7.3.23/ext/exif/

# 执行 phpize 脚本。
/usr/local/php/bin/phpize

# 编译配置。
./configure --with-php-config=/usr/local/php/bin/php-config

# 编译安装 exif 模块。
make && make install
```

3. 配置 `php.ini` 文件。可在终端中输入 `php --ini` 查看 `php.ini` 配置文件路径。如下图所示：

![](https://main.qcloudimg.com/raw/149f2f45d6c305fa8710fe265c6be57d.png)

4. 您可通过 `vim ` 命令，或使用 WinSCP 工具，根据查询到的文件路径，打开并编辑 `php.ini` 文件，去掉以下字段前面的 `;` 分号，并保存 `php.ini`  文件。如下所示

```
extension=exif    
exif.encode_unicode = ISO-8859-15
exif.decode_unicode_motorola = UCS-2BE
exif.decode_unicode_intel    = UCS-2LE
exif.encode_jis =
exif.decode_jis_motorola = JIS
exif.decode_jis_intel    = JIS=
```

5. 重启 PHP 与 Caddy 服务。
   ::: tip
   您可以参考命令 `systemctl restart php-fpm` 与 `systemctl restart caddy` 重启 PHP 与 Caddy 服务。
   ::: 

#### 安装 PHP 函数

::: tip

- Discuz! Q 依赖于 `symlink`、`readlink`、 `putenv`、 `realpath`、`shell_exec` 函数，在 PHP 中需开启以上函数。
- 以下操作因为系统、软件版本的不同，操作上会有所差异，请根据您的具体情况进行开启对应函数，以下操作仅提供示例。
  ::: 

1. 打开并编辑 `php.ini` 文件。查找 `disable_functions` 字段，并删除禁用掉的 `symlink`、`readlink`、 `putenv`、 `realpath`、`shell_exec` 函数。如下所示：

```
disable_functions = passthru,exec,system,chroot,chgrp,chown,shell_exec,proc_open,proc_get_status,popen,ini_alter,ini_restore,dl,openlog,syslog,popepassthru,stream_socket_server
```

2. 保存 `php.ini`文件,并重启 PHP 与 Caddy 服务。



#### 步骤二：下载并解压 Discuz！Q 安装包

1. 您可以通过以下命令在网站主目录下创建 Discuz！Q 网站目录并下载 Discuz！Q 安装包与解压至网站目录。

```
# 创建 Discuz！Q 网站目录，目录名可自定义，此处以 discuz 为例。
mkdir discuz

# 进入 discuz目录
cd discuz

#下载 Discuz！Q 安装包
wget -c https://dl.discuz.chat/dzq_latest_install.zip

# 解压 Discuz！Q 安装包
unzip dzq_latest_install.zip
```

#### 步骤三：配置 Caddy 

1.  您可通过 `vim` 命令，或使用 WinSCP 工具，打开 `Caddyfile` 配置文件进行以下配置。

#### root 配置

1. 请将`root`目录指向 `<站点主目录>/public`目录，配置请参考如下：
   ::: tip
   以下是示例，请按自己的实际配置设置。
   ::: 

```
root * /var/www/discuz/public
```

2. 保存 `Caddyfile`文件，重启 Caddy 服务。

   ::: tip
   如果您使用的是  Ubuntu 系统，建议您将 `caddy.service`配置文件下的 `PrivateTmb`字段配置为`false`。否则可能导致访问 Discuz! Q 时 ，随机性产生 404 页面。 
   ::: 

   

#### 步骤四：初始化安装 Discuz! Q

1. 打开本地浏览器，访问 `http://<绑定网站的域名名称>/dl.php` 。如下图所示：

![](https://main.qcloudimg.com/raw/d571ed3234565af42c508666b827ca37.png)

2. 单击【下一步】，Discuz! Q 将自行进行站点检查，如有报错等问题。可参考 [常见问题](https://discuz.com/docs/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98.html) 进行排查处理。

3. Discuz! Q 检查完成后，需配置网站相关信息。如下图所示：

![](https://main.qcloudimg.com/raw/8e3ac52669180620b946750787b50c95.png)

- **站点名称**：请输入您的站点名称信息，可自定义。
- **MySQL 服务器地址**：请输入您的 MySQL 服务器地址。
- **数据库名称**：请输入您的数据库名称。
- **MySQL 用户名**：请输入您的数据库用户名。
- **MySQL 密码**：请输入您的数据库密码。
- **表前缀**：可选，可自定义数据库表前缀名称。默认不填。
- **设置管理员用户名**：请输入您 Discuz! Q 站点的管理员用户名。
- **设置管理员密码**：请输入您 Discuz! Q 站点的管理员密码。
- **管理员密码确认**：请再次输入您 Discuz! Q 站点的管理员密码。

4. 单击【下一步】。即可完成 DIscuz！Q 的安装部署。

#### 步骤五：添加计划任务

为保证 DIscuz！Q 站点功能的正常使用。您还需要在服务器添加计划任务。在终端中输入以下命令并执行。

```
php <网站主目录>/disco schedule:run >> /dev/null 2>&1
```

##  基于宝塔

####  操作场景

本文档将指导您如何在已安装宝塔面板的 Linux 服务器上安装部署 DIscuz！Q。 

::: tip

 本文档以宝塔面板 7.3.0 版本、CentOS 7.6 系统为例。

:::

####  前提条件

- 宝塔面板安装的 PHP 版本需为 7.2 及以上，MySQL 版本需要为 MySQL 5.7.9 及以上。
- 已成功登录宝塔控制台。    

#### 操作步骤

#### 步骤一：配置PHP

#### 安装扩展

::: tip
Discuz！Q 依赖于 `fileinfo` 和 `exif` 两个扩展运行，部署前需对 PHP 进行相关配置。以下操作将指导您安装`fileinfo` 和`exif` 扩展。
:::

1. 在宝塔控制台中，单击【软件商城】->【已安装】，查找已安装的 PHP 软件。如下图所示： 

![](https://main.qcloudimg.com/raw/63a722f7fd3a159978343331d14d9736.png)

2. 单击选择需要为 Discuz！Q 使用的 PHP 软件名称（需为7.2及以上）。此处以 PHP-7.2 为例。

![](https://main.qcloudimg.com/raw/b63aed08a014d1821f75281192561aad.png)

3. 在弹出的 【php 管理】窗口中，单击【安装扩展】，进行安装扩展设置。如下图所示

![](https://main.qcloudimg.com/raw/189db4e283e426bf9d704e647c3349e3.png)

4. 选择【fileinfo】与【exif】，并单击【安装】。如下图所示：

![](https://main.qcloudimg.com/raw/35c37af44765699060f45a2b3f672df5.png)

5. 在弹出的 `fileinfo` 安装窗口中，单击【确定】。如下图所示：

![](https://main.qcloudimg.com/raw/17fec169be7dbcec963425ccf6742406.png)

6. php 管理窗口中【fileinfo】与【exif】状态栏显示为![](https://main.qcloudimg.com/raw/6afdf7dacebf13ae1615976c36da021f.png)即为安装成功。

#### 删除禁用函数

::: tip
Discuz！Q 需删除禁用的函数 `putenv`、`readlink`、`symlink`、`shell_exec` ，部署前需对 PHP 进行相关配置。以下操作将指导您删除禁用函数 `putenv`、`readlink`、`symlink`、`shell_exec` 。
::: 

1. 在 【php 管理】窗口中，单击【禁用函数】，进入禁用函数设置页面。如下图所示：

![](https://main.qcloudimg.com/raw/0c31e34664a77eeb07cc3a517b19dae3.png)

2. 在函数列表中单击【删除】函数 `putenv`、`readlink`、`symlink`、`shell_exec` ，即可删除禁用的函数。

#### 步骤二：创建站点

1. 在宝塔控制台中，单击【网站】->【添加站点】。如下图所示： 

![](https://main.qcloudimg.com/raw/59f1d176aa8cd67b6fc21bea433e802a.png)

2. 在弹出的添加网站窗口中，输入相关配置信息。如下图所示：

![](https://main.qcloudimg.com/raw/e447eebc5b027ee24615721c18085d0c.png)

- 域名：输入绑定 Discuz！Q 域名。
  ::: tip
  确保绑定的域名已添加相关解析。如您的域名使用腾讯云 DNS 解析 DNSPod ，详情操作可参考：[快速添加域名解析](https://cloud.tencent.com/document/product/302/3446)。
  ::: 
- 备注：可选，可添加网站的备注信息。
- 根目录：网站文件根目录。一般情况使用默认 `/www/wwwroot` 路径。
- FTP：可选，可创建 FTP 服务。默认不创建。
- 数据库：可选，您可在您的服务器内创建数据库进行使用。也可以其他数据库,数据库类型需为 MySQL 5.7.9 版本以上或 MariaDB 10.2 以上。如：[云数据库 TencentDB for MySQL](https://cloud.tencent.com/product/cdb)。详情操作请参考：[云数据库 MySQL 产品文档](https://cloud.tencent.com/document/product/236) 。

::: tip
如需在服务器内创建数据库进行使用，请选择【MySQL】,【utf8mb4】，并输入数据库用户名以及密码。
::: 

- 程序类型：PHP，默认不可选。
- PHP版本：请选择已配置完成的 PHP 版本。
- 网站分类：默认分类。可根据实际情况进行选择。

3.单击【提交】。即可创建站点。

#### 步骤三：远程下载 DIscuz！Q 部署文件压缩包

1. 单击已创建的 Discuz！Q 根目录路径，如下图所示：

![](https://main.qcloudimg.com/raw/ee84ac3e473b5a2662a9d6d5a72748aa.png)

2. 在文件目录中，单击【远程下载】。在弹出的下载文件窗口中输入相关信息。如下图所示：

![](https://main.qcloudimg.com/raw/6a2de56d62244bc1a6b86dc8c1e1084b.png)

- URL地址：请输入 Discuz！Q 下载地址： `https://dl.discuz.chat/dzq_latest_install.zip`。
- 下载到：默认不修改。
- 文件名：默认不修改，输入下载路径后将自动进行填充。

3. 单击【确认】。系统将自行下载 DIscuz！Q 部署文件，请耐心等待。

#### 步骤四：解压  DIscuz！Q 部署文件压缩包

1. 选择已下载好 Discuz！Q 部署文件压缩包，并单击【解压】。如下图所示：
   ![](https://main.qcloudimg.com/raw/3c54e46dcc0e4b087d65f3c8b5ac6800.png)

2. 在弹出的解压文件窗口中，单击【解压】。如下图所示：

![](https://main.qcloudimg.com/raw/27527d5113e8c2d6584e5bc2003402a7.png)

::: tip
文件解压成功后，可自行删除 Discuz！Q 部署文件压缩包，不影响 Discuz！Q 的正常使用。
::: 

#### 步骤五：修改网站配置

1. 在宝塔控制台中，单击【网站】，并选择已创建 Discuz！Q  站点，单击【设置】。如下图所示： 

![](https://main.qcloudimg.com/raw/266a9747c34988e8c1c4c438b88793c9.png)

2. 在弹出的站点修改窗口中，单击【网站目录】，进行网站目录相关设置。如下图所示： 

![](https://main.qcloudimg.com/raw/7da0693dcb1f6c27ce6f69ec2f50bbfb.png)

3. 运行目录勾选 `/public`。如下图所示:

![](https://main.qcloudimg.com/raw/30b5d8759e5d541e0c93170844bb3693.png)

4. 单击【保存】。

##### 配置伪静态

::: tip
如果使用 `Apache `服务，此步操作无需配置；如果使用 `Nginx` 服务，请按照如下进行设置伪静态。
::: 

1. 进入【网站】，单击刚才添加站点，操作栏处【设置】。
2. 弹出的【站点修改】窗口中，单击【伪静态】，将以下内容复制粘贴进去，并单击【保存】。

```
location / {
  try_files $uri $uri/ /index.php?$query_string;
}
```

#####  设置 gzip

::: tip
如果使用Apache服务，此步操作无需配置；如果使用 Nginx 服务，请按照如下进行设置 gzip。
::: 

1. 单击【软件商城】，并查找到您安装的 Nginx。
2. 单击 Nginx 操作栏的【设置】。
3. 在弹出的【nginx 管理】窗口中，单击【配置修改】。
4. 在【配置修改】中查找到 `gzip_types` 字段并在前端添加 `application/json` 和末尾处添加 `application/vnd.api+json` 代码。如下图所示：

![](https://main.qcloudimg.com/raw/74128e9f20252192ced5a0fc7a1e6ad4.png)

5. 单击 【性能调整】，在 gzip 处选择【开启】。若已开启请忽略此步操作。
6. 单击【保存】。重启 Nginx 服务。



#### 步骤六：初始化安装 Discuz! Q

1. 打开本地浏览器，访问 `http://<绑定网站的域名名称>/dl.php` 。如下图所示：

![](https://main.qcloudimg.com/raw/d571ed3234565af42c508666b827ca37.png)


::: tip

- 站点如需部署 SSL 证书，使用 HTTPS 协议进行站点访问。请在以下操作之前部署安装 SSL 证书，并使用`https://<绑定网站的域名名称>/dl.php`  进行访问。部署安装 SSL 证书详情操作请参考： [宝塔服务器证书安装](https://cloud.tencent.com/document/product/400/50874)。
- 如需购买 SSL 证书，可参考 [SSL 证书购买流程](https://cloud.tencent.com/document/product/400/7995)，请根据您的实际情况进行购买相关证书。腾讯云为 Discuz! Q 站点提供免费 SSL 证书申请。详情请查看：[域名型（DV）免费证书申请流程](https://cloud.tencent.com/document/product/400/6814)
  :::

2. 单击【下一步】，Discuz! Q 将自行进行站点检查。如有报错等问题，请查看 [常见问题](https://discuz.com/docs/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98.html) 进行排查处理。

3. Discuz! Q 检查完成后，需配置网站相关信息。如下图所示：

![](https://main.qcloudimg.com/raw/8e3ac52669180620b946750787b50c95.png)

- **站点名称**：请输入您的站点名称信息，可自定义。
- **MySQL 服务器地址**：请输入您的 MySQL 服务器地址，如您使用宝塔创建的服务器本地数据库，请输入`127.0.0.1`即可。
- **数据库名称**：请输入您的数据库名称。如您使用宝塔创建的服务器本地数据库，可登录服务器的宝塔控制台，【数据库】进行查看。
- **MySQL 用户名**：请输入您的数据库用户名。如您使用宝塔创建的服务器本地数据库，可登录服务器的宝塔控制台，【数据库】进行查看。
- **MySQL 密码**：请输入您的数据库密码。如您使用宝塔创建的服务器本地数据库，可登录服务器的宝塔控制台，【数据库】进行查看。
- **表前缀**：可选，可自定义数据库表前缀名称。默认不填。
- **设置管理员用户名**：请输入您 Discuz! Q 站点的管理员用户名。
- **设置管理员密码**：请输入您 Discuz! Q 站点的管理员密码。
- **管理员密码确认**：请再次输入您 Discuz! Q 站点的管理员密码。

4. 单击【下一步】。即可完成 DIscuz！Q 的安装部署。

#### 步骤七：添加计划任务

为保证 DIscuz！Q 站点功能的正常使用。您还需要在宝塔面板中添加计划任务。

1. 登录宝塔控制台，单击【计划任务】，进入计划任务页面，并选择与填写相关信息。如下图所示：

![](https://main.qcloudimg.com/raw/e7443e1378a3149d1de8108321b16e7e.png)

- 任务类型：选择 Shell 脚本。
- 任务名称：可自定义，建议填写【Discuz！Q 计划任务】 便于区分。
- 执行周期：选择【N分钟】，并填写【1】分钟。
- 脚本内容：请输入以下命令，命令【网站主目录】请按实际情况做相应调整。

```
sudo -u www /usr/bin/php /www/wwwroot/<网站主目录>/disco schedule:run
```

2. 单击【添加任务】。

## 基于 Docker 容器 （推荐）

#### 操作场景

本文档将指导您如何在已安装 docker-ce 运行环境的 Linux 服务器上安装部署 Discuz! Q。

::: tip
如果您的服务器还未安装 docker-ce 运行环境，腾讯云提供了 [docker 镜像](https://mirrors.tencent.com/docker-ce/)，您可直接下载安装。
:::

#### 前提条件

已成功登录 Linux 服务器。    

#### 操作步骤

#### 步骤一：安装 Discuz! Q 

1. 在终端中，请输入以下命令。docker 将会自动下载并运行最新版本的 Discuz! Q。如下所示：

```
docker run -d -p 80:80 -p 443:443 ccr.ccs.tencentyun.com/discuzq/dzq:latest
```

::: warning

- 容器基于 Ubuntu 18.04，其中已安装 Nginx 1.14, PHP 7.2, MySQL 5.7 和所有的相关依赖，并且已经完成 Web 服务器配置和计划任务配置。
- 安装时警告 `WARNING: IPv4 forwarding is disabled. Networking will not work.`。可使用命令 `vim /etc/sysctl.conf` 编辑配置文件。修改 `net.ipv4.ip_forward`字段值为 `1` 开启转发并使用命令 `systemctl restart network` 重启网络服务。
- 以上命令用于快速启动并测试 Discuz! Q，数据库和站点数据都将保存在容器内部，容器被删除将会造成数据丢失。
- 如果您想基于容器长期运行Discuz! Q，建议将数据库和站点数据保存于容器外部，请参考[容器的更多配置说明进行配置](https://discuz.com/docs/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98.html#%E5%AE%B9%E5%99%A8%E7%9A%84%E6%9B%B4%E5%A4%9A%E9%85%8D%E7%BD%AE%E8%AF%B4%E6%98%8E)。
  :::

#### 步骤二：初始化安装 Discuz! Q

1. 打开本地浏览器，访问 `http://<服务器外网 IP 地址>/install` 并配置网站相关信息。如下图所示：

![](https://s3.jpg.cm/2021/01/17/wE3Fu.png)

- **站点名称**：请输入您的站点名称信息，可自定义。
- **MySQL Host**：请输入您的 MySQL 服务器地址，如您使用 docker 创建的本地数据库，请输入`127.0.0.1`即可。
- **MySQL 数据库**：请输入您的数据库名称。如您使用 docker 创建的本地数据库，请输入`root`即可。
- **MySQL 用户名**：请输入您的数据库用户名。如您使用 docker 创建的本地数据库，请输入`root`即可。
- **MySQL 密码**：如您使用 docker 创建的本地数据库，请输入`root`即可。
- **表前缀**：可选，可自定义数据库表前缀名称。默认不填。
- **管理员 用户名**：请输入您 Discuz! Q 站点的管理员用户名。
- **管理员 密码**：请输入您 Discuz! Q 站点的管理员密码。
- **管理员 确认密码**：请再次输入您 Discuz! Q 站点的管理员密码。
::: tip
- 如果您想基于容器长期运行 Discuz! Q，建议将数据库和站点数据保存于容器外部，请参考 [容器的更多配置说明进行配置](https://discuz.com/docs/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98.html#%E5%AE%B9%E5%99%A8%E7%9A%84%E6%9B%B4%E5%A4%9A%E9%85%8D%E7%BD%AE%E8%AF%B4%E6%98%8E)。
:::

2. 单击【安装】。即可完成 DIscuz！Q 的安装部署。

#### 步骤三：系统管理与配置

安装完成后，您可以访问 `http://<服务器外网 IP 地址>/admin` 进入后台，输入在安装的时候设置的管理员账号和密码，进行管理与配置。





